//
//  Trailer.swift
//  TrailerFlix
//
//  Created by Gerson Costa on 10/12/2018.
//  Copyright © 2018 Gerson Costa. All rights reserved.
//

import Foundation

struct Trailer: Codable {
    
    let title: String
    let url: String
    let rating: Int
    let year: Int
    let poster: String
    
}
